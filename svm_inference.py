from joblib import load
from sklearn import datasets
from sklearn import metrics
import numpy as np
import time

# Load the MNIST dataset
digits = datasets.load_digits()

# Split the data into training and testing sets
n_samples = len(digits.images)
data = digits.images.reshape((n_samples, -1))
X_test, y_test = data[n_samples // 2:], digits.target[n_samples // 2:]

# Load the saved SVM classifier
loaded_classifier = load('svm_model_quantized.joblib')

# Quantize the test data to 16-bit precision
X_test_quantized = np.float16(X_test)

# Make predictions on the quantized test set using the loaded model
y_pred_loaded = loaded_classifier.predict(X_test_quantized)

# Calculate the accuracy
accuracy_loaded = metrics.accuracy_score(y_test, y_pred_loaded)
print(f'Accuracy (Loaded Model with Quantized Data): {accuracy_loaded * 100:.2f}%')
